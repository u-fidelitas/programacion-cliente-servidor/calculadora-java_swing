package BizLogic;

/**
 *
 * @author fabianfdz
 */
public class Calculator {
    Double accumulated;
    String action;

    public Calculator() {
        accumulated = 0.0;
        action = "";
    }
    
    public void add(Double num) {
        accumulated+=num;
        action = "+";
    }
    
    public void minus(Double num) {
        accumulated-=num;
        action = "-";
    }
    
    public void multiply(Double num) {
        accumulated*=num;
        action = "*";
    }
    
    public void divide(Double num) {
        accumulated/=num;
        action = "/";
    }
    
    public void power(Double num) {
        accumulated = num;
        action = "^";
    }

    public Double getAccumulated() {
        return accumulated;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
    
    public void clearAll() {
        action = "";
        accumulated = 0.0;
    }
    
    public Double result(Double lastNum, String action) {
        Double res;
        switch(this.action) {
            case "+":
                res = accumulated + lastNum;
                // this.action = "";
                break;
            case "-":
                res = accumulated - lastNum;
                // this.action = "";
                break;
            case "*":
                res = accumulated * lastNum;
                // this.action = "";
                break;
            case "/":
                res = accumulated / lastNum;
                // this.action = "";
                break;
            case "^":
                res = Math.pow(accumulated, lastNum);
                // this.action = "";
                break;
            default:
                res = lastNum;
                // this.action = action;
        }
        
        this.action = action;
        accumulated = res;
        
        return res;
    }
}
